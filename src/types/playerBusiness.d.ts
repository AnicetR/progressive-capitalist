interface PlayerBusiness {
    readonly id: string;
    readonly count: number;
    readonly hasManager: boolean;
    readonly lastStart: number | null;
}
