interface State {
    businesses: ReadonlyArray<PlayerBusiness>;
    money: number;
}
