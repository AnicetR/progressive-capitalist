interface Business {
    readonly id: string;
    readonly name: string;
    readonly price: number;
    readonly revenue: number;
    readonly time: number;
    readonly manager: Manager;
}
