import {
    useState,
    useEffect,
    useCallback,
    useRef,
} from 'react';

const useKonamiCode = (): Array<any> => {
    const [konamiCode, setIsKonami] = useState(false);
    const refIndex = useRef(0);

    const onKeyUpCallback = useCallback((e) => {
        const sequence = [38, 38, 40, 40, 37, 39, 37, 39, 66, 65];

        const onKeyUp = (ke: KeyboardEvent): void => {
            const { keyCode } = ke;

            if (refIndex.current === sequence.length - 1) {
                setIsKonami(true);
                refIndex.current = 0;
                return;
            }

            if (keyCode !== null && sequence[refIndex.current] !== null && keyCode === sequence[refIndex.current]) {
                refIndex.current += 1;
                return;
            }

            refIndex.current = 0;
            setIsKonami(false);
        };

        onKeyUp(e);
    }, []);

    useEffect(() => {
        window.addEventListener('keyup', onKeyUpCallback);

        return () => window.removeEventListener('keyup', onKeyUpCallback);
    }, [onKeyUpCallback]);

    return [konamiCode, setIsKonami];
};

export default useKonamiCode;
