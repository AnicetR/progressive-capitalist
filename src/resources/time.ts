const formatCountdown = (timeLeft: number): string => {
    const timeLeftInSec = Math.floor(timeLeft / 1000);
    if (timeLeftInSec < 0) {
        return '';
    }

    const seconds = Math.floor((timeLeft % (1000 * 60)) / 1000);

    if (timeLeftInSec <= 60) {
        return `${seconds}s`;
    }

    const minutes = Math.floor((timeLeft % (1000 * 60 * 60)) / (1000 * 60));
    if (timeLeftInSec <= 60 * 60) {
        return `${minutes}m ${seconds}s`;
    }

    const hours = Math.floor((timeLeft % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));

    return `${hours}h ${minutes}m ${seconds}s`;
};

const getSecondsLeft = (lastStart: number, time: number): number => {
    const targetTime = lastStart + (time * 1000);
    const currentTime = +new Date();

    return Math.ceil(targetTime - currentTime);
};

const getProgress = (secondsLeft: number, time: number): number => {
    if (secondsLeft <= 0) {
        return 0;
    }

    const progress = (((time * 1000) - secondsLeft) * 100) / (time * 1000);

    return progress < 0 ? 0 : progress;
};

export {
    formatCountdown,
    getSecondsLeft,
    getProgress,
};
