const getBusinessById = (
    id: string,
    playerBusinesses: ReadonlyArray<PlayerBusiness>,
): PlayerBusiness | undefined => playerBusinesses.find((b) => b.id === id);

const countBusinesses = (id: string, playerBusinesses: ReadonlyArray<PlayerBusiness>): number => {
    const business = getBusinessById(id, playerBusinesses);
    if (business === undefined) {
        return 0;
    }

    return business.count;
};

const addBusinessToPlayer = (
    business: Business,
    playerBusinesses: ReadonlyArray<PlayerBusiness>,
): ReadonlyArray<PlayerBusiness> => {
    const playerBusiness = getBusinessById(business.id, playerBusinesses);
    if (playerBusiness === undefined) {
        return [
            ...playerBusinesses,
            {
                id: business.id,
                count: 1,
                hasManager: false,
                lastStart: null,
            },
        ];
    }

    return playerBusinesses.map((b) => {
        if (b.id === business.id) {
            return {
                ...b,
                count: b.count + 1,
            };
        }

        return b;
    });
};

const addManagerToPlayerBusiness = (
    id: string,
    playerBusinesses: ReadonlyArray<PlayerBusiness>,
): ReadonlyArray<PlayerBusiness> => {
    const playerBusiness = getBusinessById(id, playerBusinesses);
    if (playerBusiness === undefined) {
        return playerBusinesses;
    }

    return playerBusinesses.map((b) => {
        if (b.id === id) {
            return {
                ...b,
                hasManager: true,
            };
        }

        return b;
    });
};

const updateLastStart = (
    id: string,
    playerBusinesses: ReadonlyArray<PlayerBusiness>,
    timestamp: number | null = 0,
): ReadonlyArray<PlayerBusiness> => {
    const playerBusiness = getBusinessById(id, playerBusinesses);
    if (playerBusiness === undefined) {
        return playerBusinesses;
    }

    return playerBusinesses.map((b) => {
        if (b.id === id) {
            return {
                ...b,
                lastStart: timestamp === 0 ? +new Date() : timestamp,
            };
        }

        return b;
    });
};

export {
    getBusinessById,
    countBusinesses,
    addBusinessToPlayer,
    addManagerToPlayerBusiness,
    updateLastStart,
};
