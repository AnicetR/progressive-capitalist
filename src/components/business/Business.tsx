import React, { useState, useMemo } from 'react';
import classnames from 'classnames/bind';

import Button from 'components/button/Button';

import useInterval from 'hooks/useInterval';

import getConfig from 'resources/config';
import { getBusinessById } from 'resources/business';
import { abvNumber, calculateEarnings } from 'resources/money';
import { formatCountdown, getSecondsLeft, getProgress } from 'resources/time';

import businessImages from 'images/businesses';

import styles from './Business.scss';

interface Props {
    readonly id: string;
    readonly name: string;
    readonly price: number;
    readonly revenue: number;
    readonly time: number;
    readonly playerMoney: number;
    readonly playerBusinesses: ReadonlyArray<PlayerBusiness>;
    readonly onBuyBusiness: (event: React.MouseEvent<HTMLButtonElement | HTMLDivElement>) => void;
    readonly onWorkBusiness: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const cx = classnames.bind(styles);
const {
    currency,
    maxBusinesses,
    priceIncrease,
} = getConfig();

const Business: React.FunctionComponent<Props> = ({
    id,
    name,
    price,
    revenue,
    time,
    playerMoney,
    playerBusinesses,
    onBuyBusiness,
    onWorkBusiness,
}) => {
    const playerBusiness = useMemo(() => getBusinessById(id, playerBusinesses), [id, playerBusinesses]);
    const [timeLeft, setTimeLeft] = useState(0);

    useInterval(() => {
        if (playerBusiness?.lastStart === undefined || playerBusiness.lastStart === null) {
            return;
        }

        setTimeLeft(getSecondsLeft(playerBusiness.lastStart, time));
    }, 200);

    // If the business is not owned
    if (playerBusiness === undefined) {
        // Check if the business can be bought
        const isAvailable = playerMoney >= price;

        return (
            <div className={cx('wrapper', 'forSale', { available: isAvailable })}>
                <span className={cx('name')}>{ name }</span>
                <span className={cx('price')}>
                    { abvNumber(price) }
                    &nbsp;
                    { currency }
                </span>
                <Button
                    type={isAvailable ? 'success' : 'error'}
                    data-id={id}
                    data-price={price}
                    onClick={onBuyBusiness}
                    isDisabled={!isAvailable}
                >
                    Buy now!
                </Button>
            </div>
        );
    }

    const {
        count,
        lastStart,
        hasManager,
    } = playerBusiness || {};

    const cost = price + (price * (priceIncrease * count));
    const progress = getProgress(timeLeft, time);
    const hasMaxBusiness = count >= maxBusinesses;

    return (
        <div className={cx('wrapper', 'owned')}>
            <div className={cx('icon')}>
                <img src={businessImages[id]} alt={name} />
                { hasManager && (
                    <span className={cx('manager')} />
                ) }
                <span className={cx('count')}>
                    {count}
                </span>
            </div>
            <div className={cx('infos')}>
                <span className={cx('name')}>{ name }</span>
                <div className={cx('progress')}>
                    <div className={cx('bar')} style={{ width: `${lastStart === null ? 0 : progress}%` }} />
                    <span className={cx('timer')}>
                        { !hasManager && lastStart === null
                            ? 'Click "Work" to earn!'
                            : `${abvNumber(calculateEarnings(revenue, count))} ${currency} in ${formatCountdown(timeLeft)}` }
                    </span>
                </div>
                <div className={cx('actions')}>
                    <Button
                        type="success"
                        data-id={id}
                        data-price={cost}
                        onClick={onBuyBusiness}
                        isDisabled={hasMaxBusiness || playerMoney < cost}
                    >
                        { hasMaxBusiness
                            ? 'Sold out'
                            : `Buy ${abvNumber(cost)} ${currency}` }
                    </Button>
                    <Button
                        type="success"
                        data-id={id}
                        onClick={onWorkBusiness}
                        isDisabled={hasManager || lastStart !== null}
                    >
                        { !hasManager && lastStart === null
                            ? `Work | ${abvNumber(calculateEarnings(revenue, count))} ${currency}`
                            : 'Working...' }
                    </Button>
                </div>
            </div>
        </div>
    );
};

export default Business;
