import React from 'react';
import classnames from 'classnames/bind';

import travolta from 'images/confused-travolta.gif';

import styles from './Egg.scss';

const cx = classnames.bind(styles);

const Egg = () => (
    <img className={cx('bottom')} src={travolta} alt="Where's the easter egg?" />
);

export default Egg;
