import React from 'react';
import classnames from 'classnames/bind';

import { Views } from 'resources/constants';
import { abvNumber } from 'resources/money';
import getConfig from 'resources/config';

// Images
import backImage from 'images/header/back.png';
import managersImage from 'images/header/managers.png';
import settingsImage from 'images/header/settings.png';

import styles from './Header.scss';

interface Props {
    readonly money: number;
    readonly view: number;
    readonly onSetView: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const cx = classnames.bind(styles);
const { currency } = getConfig();

const Header: React.FunctionComponent<Props> = ({
    money,
    view,
    onSetView,
}) => (
    <div className={cx('wrapper')}>
        <span className={cx('managers')}>
            {view === Views.BUSINESSES && (
                <button
                    className={cx('btn')}
                    onClick={onSetView}
                    data-id={Views.MANAGERS}
                    type="button"
                >
                    <img src={managersImage} alt="Hire managers" />
                </button>
            )}
            {(view === Views.MANAGERS || view === Views.SETTINGS) && (
                <button
                    className={cx('btn')}
                    onClick={onSetView}
                    data-id={Views.BUSINESSES}
                    type="button"
                >
                    <img src={backImage} alt="Back to businesses" />
                </button>
            )}
        </span>
        <span className={cx('money')}>
            { abvNumber(money) }
            &nbsp;
            { currency }
        </span>
        <span className={cx('settings')}>
            {view === Views.BUSINESSES && (
                <button
                    className={cx('btn')}
                    onClick={onSetView}
                    data-id={Views.SETTINGS}
                    type="button"
                >
                    <img src={settingsImage} alt="Settings" />
                </button>
            )}
        </span>
    </div>
);

export default Header;
